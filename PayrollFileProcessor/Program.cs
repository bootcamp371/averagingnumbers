﻿using System;
using System.IO;

namespace PayrollFileProcessor
{
    class Program
    {
        static void Main(string[] args)
        {
            string filePath = @"C:\Users\cn220029\directorysearch\";
            string fileName = "";
            string fileNameHold;
            
            double payRate;
            double hours;
            string[] inputLine;
            string outputFileLoc = @"C:\Users\cn220029\directorysearch\PayrollFileProcessor.log";
            double totalPay = 0;



            Console.Write("Enter the file name you would like to read: ");
            fileName = Console.ReadLine();
            fileNameHold = fileName;
            fileName = filePath + fileName;

            if (!File.Exists(fileName))
            {
                Console.WriteLine(
                $"Error: {fileName} doesn't exist!");
                return;
            }

            try
            {
                using (StreamReader inputFile = new StreamReader(fileName))
                {
                    while (inputFile.EndOfStream != true)
                    {
                        string text = inputFile.ReadLine();
                        Console.WriteLine(text);

                        inputLine = text.Split("~");
                        
                        payRate = Convert.ToDouble(inputLine[2]);
                        hours = Convert.ToDouble(inputLine[3]);

                        totalPay = totalPay + (payRate * hours);

                       /* Console.WriteLine("Employee Number: " + inputLine[0]);
                        Console.WriteLine("Employee Name: " + inputLine[1]);
                        Console.WriteLine("Pay Rate: " + inputLine[2]);
                        Console.WriteLine("Hours Worked: " + inputLine[3]);*/

                        
                    }
                    using (StreamWriter outputFile = new StreamWriter(outputFileLoc, true))
                    {
                        outputFile.WriteLine(
                        "Payroll from " + fileNameHold + " processed On {0:MMMM d, yyyy} at {0:h:mm:ss} " +
                        "gross pay was {1}",
                        DateTime.Now, totalPay);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error opening file: {ex.Message}");
            }


        }
    }
}
