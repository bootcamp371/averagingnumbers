﻿using System;

namespace GregorianCalendar
{
    class Program
    {
        static void Main(string[] args)
        {
            int year;
            string leapYear;
            int remainder;
            Console.Write("Enter in a year: ");
            year = Convert.ToInt32(Console.ReadLine());

            remainder = year % 4;
            leapYear = (remainder == 0) ? "is" : "is not";
            Console.WriteLine("{0} {1} a leap year", year, leapYear);


        }
    }
}
