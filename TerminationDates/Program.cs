﻿using System;

namespace TerminationDates
{
    class Program
    {
        public static object D2 { get; private set; }

        static void Main(string[] args)
        {
            string renewalString;
            DateTime renewalDate;
            DateTime graceDate;
            DateTime cancelDate;
            string graceString;
            string cancelString;

            Console.Write("Please enter in a policy renewal date: ");
            renewalString = Console.ReadLine();

            renewalDate = Convert.ToDateTime(renewalString);
            graceDate = renewalDate.AddDays(10);
            cancelDate = renewalDate.AddMonths(1);

            graceString = graceDate.ToString("MM/dd/yyyy");
            cancelString = cancelDate.ToString("MM/dd/yyyy");

            Console.WriteLine("Grace period ends on: {0}", graceString);
            Console.WriteLine("Lack of Payment cancel date: {0}", cancelString);

            Console.WriteLine($"Grace period ends on: {graceDate:MM/dd/yyyy}");
            Console.WriteLine($"Lack of Payment cancel date: {cancelDate:MM/dd/yyyy}");

            Console.WriteLine($"Grace period ends on: {graceDate.Month:D2}/{graceDate.Day:D2}/{graceDate.Year}");
            Console.WriteLine($"Lack of Payment cancel date:{cancelDate.Month:D2}/{cancelDate.Day:D2}/{cancelDate.Year}");


        }
    }
}
