﻿using System;

namespace MortgageCalculator
{
    class Program
    {
        static void Main(string[] args)
        {
            string again = "N";
            do
            {
                decimal A = 0;
                decimal MIR = 0;
                int NP = 0;
                double Pmt;
                Console.Write("How much are you borrowing? ");
                try
                {
                    A = Convert.ToDecimal(Console.ReadLine());
                }
                catch (FormatException ex)
                {
                    Console.WriteLine("Incorrect format for how much you are borrowing." + ex);
                    break;
                }
                Console.Write("What is your interest rate? ");
                try
                {
                    MIR = Convert.ToDecimal(Console.ReadLine());
                }
                catch (FormatException ex)
                {
                    Console.WriteLine("Incorrect format for how much you are borrowing." + ex);
                    break;
                }
                Console.Write("How long is your payment in years? ");

                try
                {
                    NP = Convert.ToInt32(Console.ReadLine());
                }
                catch (FormatException ex)
                {
                    Console.WriteLine("Incorrect format for how much you are borrowing." + ex);
                    break;
                }

                MIR = MIR / 1200;
                NP = NP * 12 * -1;
                double power = 1 + (double)MIR;
                double numerator = (double)(A * MIR);
                double denominator = 1 - Math.Pow(power, NP);
                Pmt = numerator / denominator;

                Console.WriteLine(power);
                Console.WriteLine(numerator);

                double formulaPayment;
                formulaPayment = (double)(A * MIR) / (1 - Math.Pow((double)1 + (double)MIR, NP));

                double totalPayments = Pmt * 15 * 12;
                double totalInterest = totalPayments - (double)A;

                Console.WriteLine("Your estimated payment is {0:C}", Pmt);
                Console.WriteLine("Your estimated payment (formula) is {0:C}", formulaPayment);
                Console.WriteLine("You paid {0:C} over the life of the loan", totalPayments);
                Console.WriteLine("Your total interest cost for the load was {0:C}", totalInterest);

                Console.WriteLine("Would you like to calculate another payment? (Y/N) ");
                again = Console.ReadLine();


            } while (again == "Y") ;
        }
    }
}
