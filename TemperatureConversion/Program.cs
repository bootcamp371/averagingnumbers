﻿using System;

namespace TemperatureConversion
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            double tempF = 69.5;
            
            Console.Write("Enter in the temperature in F: ");
            tempF = Convert.ToDouble(Console.ReadLine());
            double tempC = (tempF - 32) * (.5556);
            Console.WriteLine("The temperature in F is " + tempF);
            Console.WriteLine("The temperature in C is " + tempC);

        } 
    }
}
