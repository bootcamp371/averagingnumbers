﻿using System;

namespace StatisticsManager
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] scores = { 75, 82, 85, 87, 95, 73, 100 };
            double aveScore;
            double largeScore;
            double lowScore;
            double medianScore;

            Console.WriteLine("Hello World!");
            aveScore = GetAverage(scores);
            Console.WriteLine("The average score is: " + aveScore);
            medianScore = GetMedian(scores);
            Console.WriteLine("The Median score is: " + medianScore);
            largeScore = GetLargestValue(scores);
            Console.WriteLine("The largest score is: " + largeScore);
            lowScore = GetSmallestValue(scores);
            Console.WriteLine("The lowest score is: " + lowScore);
        }
        public static double GetAverage(double[] scores)
        {
            double total = 0;
            for (int i = 0; i < scores.Length; i++)
            {
                total = total + scores[i];
            }
            return total / scores.Length;

        }
        public static double GetMedian(double[] scores)
        {
            Array.Sort(scores);
            return scores[3];
        }
        public static double GetLargestValue(double[] scores)
        {
            Array.Sort(scores);
            return scores[scores.Length - 1];
        }
        public static double GetSmallestValue(double[] scores)
        {
            Array.Sort(scores);
            return scores[0];
        }
    }
}
