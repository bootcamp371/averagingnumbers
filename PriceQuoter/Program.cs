﻿using System;

namespace PriceQuoter
{
    class Program
    {
        static void Main(string[] args)
        {
            string productCode;
            int orderQuantity;
            double pricePerQty = 0;
            string command = "";

            do
            {
                Console.Write("Enter Get-Quote to place an order or Quit to Quit the program: ");
                command = Console.ReadLine();
                if (command == "Get-Quote")
                {
                    Console.Write("Enter product Code: ");
                    productCode = Console.ReadLine();
                    Console.Write("Enter order quantity: ");
                    orderQuantity = Convert.ToInt32(Console.ReadLine());

                    if (productCode == "BG-127")
                    {
                        if (orderQuantity <= 24)
                        {
                            pricePerQty = 18.99;
                        }
                        else
                        if (orderQuantity >= 25 && orderQuantity <= 50)
                        {
                            pricePerQty = 17;
                        }
                        else
                        {
                            pricePerQty = 14.49;
                        }
                    }
                    if (productCode == "WRTR-28")
                    {
                        if (orderQuantity <= 24)
                        {
                            pricePerQty = 125.00;
                        }
                        else
                        if (orderQuantity >= 25 && orderQuantity <= 50)
                        {
                            pricePerQty = 113.75;
                        }
                        else
                        {
                            pricePerQty = 99.99;
                        }
                    }
                    if (productCode == "GUAC-8")
                    {
                        switch (orderQuantity)
                        {
                            case <= 50:
                                pricePerQty = 8.99;
                                break;
                            case >= 51:
                                pricePerQty = 7.49;
                                break;
                        }
                    }

                    if (orderQuantity > 250)
                    {
                        pricePerQty = (double)pricePerQty * .85;
                    }


                    Console.WriteLine("Product Code {0}", productCode);
                    Console.WriteLine("Quantity Ordered {0}", orderQuantity);
                    Console.WriteLine("Price per Quantity {0:c}", pricePerQty);
                } else
                if (command != "Quit")
                {
                    Console.WriteLine("Error - invalid command");
                }
            } while (command != "Quit");
        }
    }
}
