﻿using System;

namespace StudentAdvising
{
    class Program
    {
        static void Main(string[] args)
        {
            string name;
            string major;
            string classification;
            string again = "N";

            do
            {
                GatherInfo(out name, out major, out classification);
                GetLocation(major, classification);
                Console.Write("Would you like to enter another? ");
                again = Console.ReadLine();
            } while (again == "Y");
        }

        public static void GatherInfo(out string name, out string major, out string classification)
        {
            Console.Write("Enter Student Name: ");
            name = Console.ReadLine();
            Console.Write("Enter Student Major: ");
            major = Console.ReadLine();
            Console.Write("Enter Student Class: ");
            classification = Console.ReadLine();

        }
        public static void GetLocation(string major, string classification)
        {
            string location = "";
            string majorName = "";
            if (major == "BIOL")
            {
                majorName = "Biology";
                switch (classification)
                {
                    case "Freshman":
                    case "Sophomore":
                        {
                            location = "Science Building Room 310";
                            break;
                        }
                    case "Junior":
                    case "Senior":
                        {
                            location = "Science Building Room 311";
                            break;
                        }
                }

            } else
            if ( major == "CSCI"){
                majorName = "Computer Science";
                location = "Sheppard Hall Room 314";
            } else
            if (major == "ENG")
            {
                majorName = "English";
                switch (classification)
                {
                    case "Freshman":
                        {
                            location = "Kerr Hall Room 201";
                            break;
                        }
                    case "Sophomore":
                    case "Junior":
                    case "Senior":
                        {
                            location = "Kerr Hall Room 312";
                            break;
                        }
                }
            }
            if (major == "HIS")
            {
                majorName = "History";
                location = "Kerr Hall Room 114";
            } else
            if (major == "MKT")
            {
                majorName = "Marketing";
                switch (classification)
                {
                    case "Freshman":
                    case "Sophomore":
                    case "Junior":
                        {
                            location = "Westly Hall Room 310";
                            break;
                        }
                    case "Senior":
                        {
                            location = "Kerr Hall Room 312";
                            break;
                        }
                }
            }

            Console.WriteLine("Advising for {0} {1} majors: {2}", majorName, classification, location);

        }
    }

}
