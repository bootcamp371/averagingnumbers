﻿using System;

namespace Searcher
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] items = {"Sausage Breakfast Taco",
                              "Potato and Egg Breakfast Taco", "Sausage and Egg Biscuit",
                              "Bacon and Egg Biscuit", "Pancakes"};
            decimal[] prices = { 3.99M, 3.29M, 3.70M, 3.99M, 4.79M };
            decimal? result = null;
            string order = "";
            string again = "N";

            Console.WriteLine("Hello World!");

            do
            {
                result = null;
                Console.Write("Place an order: ");
                order = Console.ReadLine();
                result = getPrice(items, prices, order);

                if (result == null)
                {
                    Console.WriteLine("That item is not on the menu");
                } else
                {
                    Console.WriteLine(order + " costs " + result);
                }
                Console.Write("Would you like to order something else? ");
                again = Console.ReadLine();

            } while (again == "Y");
        }

        public static decimal? getPrice(string[] items, decimal[] prices, string ordered)
        {
            int indexOfOrdered = -1;
            
            for (int i = 0; i < items.Length; i++)
            {
                if (items[i] == ordered)
                {
                    indexOfOrdered = i;
                }
            }

            if (indexOfOrdered != -1)
            {
                return prices[1];
            } else
            {
                return null;
            }
        }
    }
}
    
