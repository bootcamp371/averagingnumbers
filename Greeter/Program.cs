﻿using System;

namespace Greeter
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime whenIsNow = DateTime.Now;
            int theHour;
            string message = "";

            theHour = whenIsNow.Hour;

            if (theHour >= 5 && 
                theHour < 10){
                message = "Good Morning";
            } else
            if (theHour >= 10 &&
                theHour < 18)
            {
                message = "Good Day";
            }
            else
            if (theHour >= 18 &&
                theHour < 24)
            {
                message = "Good Evening";
            }
            else
            {
                message = "Welcome to the late Shift";
            }

            Console.WriteLine("The hour is: " + theHour);
            Console.WriteLine(message);
        }
    }
}
