﻿using System;
using System.IO;

namespace CarDealership
{
    class Program
    {
        static void Main(string[] args)
        {
            string command;
            do
            {
                string commandList = "1,2,3,4,5,6";
                int commandIndex = -1;

                Console.WriteLine("What would you like to do? ");
                Console.WriteLine("1 - Find vehicles that match make/model");
                Console.WriteLine("2 - Find vehicles that fall within a price range");
                Console.WriteLine("3 - Find vehicles that match a color");
                Console.WriteLine("4 - Show all vehicles");
                Console.WriteLine("5 - Add a vehicle");
                Console.WriteLine("6 - Quit");
                Console.Write("Enter your command: ");
                command = Console.ReadLine();

                commandIndex = commandList.IndexOf(command);
                
                if (commandIndex == -1)
                {
                    Console.WriteLine("Invalid command, try again");
                    command = "Retry";
                } else
                if (command == "5")
                {
                    AddCar();
                    
                }
                else
                if (command == "6")
                {
                    return;
                }
                else
                {
                    GenerateCarList(command);
                }
            } while (command != "6");
            return;
        }
        static void AddCar()
        {
            int totalLines = 0;
            string fileName = @"C:\Users\cn220029\directorysearch\carlist.txt";

            Console.WriteLine("New Vehicle Entry");
            Console.WriteLine("-----------------");
            Console.Write("Enter Make : ");
            string make = Console.ReadLine();
            Console.Write("Enter Model: ");
            string model = Console.ReadLine();
            Console.Write("Enter Color: ");
            string color = Console.ReadLine();
            Console.Write("Enter Miles: ");
            string miles = Console.ReadLine();
            Console.Write("Enter Price: ");
            string price = Console.ReadLine();

            using (StreamReader inputFile = new StreamReader(fileName))
            {
                while (inputFile.EndOfStream != true)
                {
                    string text = inputFile.ReadLine();
                    totalLines++;
                }
            }
            
            totalLines++;
            using (StreamWriter outputFile = new StreamWriter(fileName, true))
            {
                
                outputFile.WriteLine("{0},{1},{2},{3},{4},{5}",totalLines, make, model, color, miles, price);

            }
            Console.WriteLine("New Car Added, back to Main Menu");
            return;
        }
        static void GenerateCarList(string command)
        {
            string searchValue = "";
            int searchIndex = 0;
            string makeOrModel;
            int minValue = 0;
            int maxValue = 0;
            bool foundOne = false;
            int validSelection = -1;
            switch (command)
            {
                case "1":
                    {
                        do
                        {
                            Console.Write("Search by Mak(e) or Mode(l): ");
                            makeOrModel = Console.ReadLine();

                            if (makeOrModel != "e" && makeOrModel != "l")
                            {
                                Console.WriteLine("Invalid selection, try again");
                                
                            }

                        } while (makeOrModel != "e" && makeOrModel != "l");

                        if (makeOrModel == "e")
                        {
                            Console.Write("Enter in the Make you are looking for: ");
                            searchValue = Console.ReadLine();
                            searchIndex = 1;
                        }
                        if (makeOrModel == "l")
                        {
                            Console.Write("Enter in the Model you are looking for: ");
                            searchValue = Console.ReadLine();
                            searchIndex = 2;
                        }
                        break;

                    }
                case "2":
                    {
                        Console.Write("Enter in your minimum value: ");
                        minValue = Convert.ToInt32(Console.ReadLine());
                        Console.Write("Enter in your max value: ");
                        maxValue = Convert.ToInt32(Console.ReadLine());
                        searchIndex = 5;
                        break;
                    }
                case "3":
                    {
                        Console.Write("Enter in the color you are looking for: ");
                        searchValue = Console.ReadLine();
                        searchIndex = 3;
                        break;
                    }
                case "4":
                    {
                        searchIndex = 8;
                        break;
                    }
            }

            string[,]? carlist = CreateArray();
            string filePath = @"C:\Users\cn220029\directorysearch\carlist.txt";
            string carInfo;

            carlist = ReadFile(filePath, carlist);


            if (searchIndex == 1 || //make
                searchIndex == 2 || //model
                searchIndex == 3)   //color
            {
                for (int i = 0; i < carlist.GetLength(0); i++)
                {
                    if (carlist[i, searchIndex] == searchValue)
                    {
                        foundOne = true;
                        Console.Write("{0, 2}", carlist[i, 0]);
                        Console.Write("{0,12}", carlist[i, 1]);
                        Console.Write("{0,10}", carlist[i, 2]);
                        Console.WriteLine();
                    }
                }
                if (foundOne == false)
                {
                    Console.WriteLine("Sorry, no cars match your search, try again!");
                    return;
                }
            } else
            if (searchIndex == 8){ //all
                for (int i = 0; i < carlist.GetLength(0); i++)
                {
                    Console.Write("{0, 2}", carlist[i, 0]);
                    Console.Write("{0,12}", carlist[i, 1]);
                    Console.Write("{0,10}", carlist[i, 2]);
                    Console.WriteLine();
                }
            } else
            if (searchIndex == 5) //price
            {
                for (int i = 0; i < carlist.GetLength(0); i++)
                {
                    if (Convert.ToInt32(carlist[i, searchIndex]) >= minValue &&
                        Convert.ToInt32(carlist[i, searchIndex]) <= maxValue)
                    {
                        foundOne = true;
                        Console.Write("{0, 2}", carlist[i, 0]);
                        Console.Write("{0,12}", carlist[i, 1]);
                        Console.Write("{0,10}", carlist[i, 2]);
                        Console.WriteLine();
                    }
                }
                if (foundOne == false)
                {
                    Console.WriteLine("Sorry, no cars match your search, try again!");
                    return;
                }
            }

            Console.Write("Enter the car you would like more info on: ");
            carInfo = Console.ReadLine();

            try
            {
                validSelection = Convert.ToInt32(carInfo);
            }
            catch (FormatException )
            {
                Console.WriteLine("This is not valid input, try again");
                return;
            }
            
            if (validSelection > carlist.GetLength(0))
            {
                Console.WriteLine("This is not valid input.");
                return;
            }

            CarDetail(carInfo, carlist);

        }
        static void CarDetail(string carInfo, string[,]carlist)
        {
            int carInt = Convert.ToInt32(carInfo) - 1;
            Console.WriteLine(" Make: " + carlist[carInt, 1]);
            Console.WriteLine("Model: " + carlist[carInt, 2]);
            Console.WriteLine("Color: " + carlist[carInt, 3]);
            Console.WriteLine("Miles: " + carlist[carInt, 4]);
            Console.WriteLine("Price: " + carlist[carInt, 5]);

            Console.Write("Would you like to (P)urchase the car or (R)eturn to the main menu? ");
            string answer = Console.ReadLine();

            if (answer != "P")
            {
                return;
            }

            Console.Write("Enter Your Name: ");
            string name = Console.ReadLine();
            Console.Write("Enter Your Street Address: ");
            string streetAddress = Console.ReadLine();

            GenerateInvoice(carInt, carlist, name, streetAddress);

            return;

        }

        static void GenerateInvoice(int carInt, string[,] carlist, string name, string streetAddress)
        {
            string outputFileLoc = @"C:\Users\cn220029\directorysearch\carInvoice.txt";
            using (StreamWriter outputFile = new StreamWriter(outputFileLoc, true))
            {
                outputFile.WriteLine("Congratulations on your new car!");
                outputFile.WriteLine("--------------------------------");
                outputFile.WriteLine("Name   :" + name);
                outputFile.WriteLine("Address:" + streetAddress);
                outputFile.WriteLine("--------------------------------");
                outputFile.WriteLine("Make : " + carlist[carInt, 1]);
                outputFile.WriteLine("Model: " + carlist[carInt, 2]);
                outputFile.WriteLine("Color: " + carlist[carInt, 3]);
                outputFile.WriteLine("Miles: " + carlist[carInt, 4]);
                outputFile.WriteLine("Price: " + carlist[carInt, 5]);
            }
            return;
        }
        static string[,]? ReadFile(string filePath, string[,] carlist)
        {
            string fileName = filePath;

            if (!File.Exists(fileName))
            {
                Console.WriteLine(
                $"Error: {fileName} doesn't exist!");
                return null;
            }

            try
            {
                using (StreamReader inputFile = new StreamReader(fileName))
                {
                    int i = 0;
                    string[] car;
                    while (inputFile.EndOfStream != true)
                    {
                        string text = inputFile.ReadLine();
                        //Console.WriteLine(text);
                        car = text.Split(",");

                        for (int j = 0; j < car.Length; j++)
                        {
                            carlist[i, j] = car[j];
                        }
                        i++;
                    }
                }
                return carlist;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error opening file: {ex.Message}");
            }
            return null;

        }
        static public string[,] CreateArray()
        {
            int totalLines = 0;
            string fileName = @"C:\Users\cn220029\directorysearch\carlist.txt";
            using (StreamReader inputFile = new StreamReader(fileName))
            {
                while (inputFile.EndOfStream != true)
                {
                    string text = inputFile.ReadLine();
                    totalLines++;
                }
            }
            string[,] table = new string[totalLines, 6];
            return table;
        }
    }
}
