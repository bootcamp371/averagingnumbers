﻿using System;

namespace timeAdd
{
    class Program
    {
        static void Main(string[] args)
        {
            int hour;
            int minutes;
            int addMinutes;
            int addHours;
            int ExtraMinutes;

            Console.Write("Enter in the hour: ");
            hour = Convert.ToInt32(Console.ReadLine());
            Console.Write("Enter in the minutes after the hour: ");
            minutes = Convert.ToInt32(Console.ReadLine());
            Console.Write("How many minutes would you like to add: ");
            addMinutes = Convert.ToInt32(Console.ReadLine());

            addHours = (addMinutes + minutes)  / 60;
            ExtraMinutes = (minutes + addMinutes) % 60;


            Console.WriteLine("The new hour is {0}", (hour + addHours) % 24);
            Console.WriteLine("The new minutes after hour is {0}", ExtraMinutes);



        }
    }
}
