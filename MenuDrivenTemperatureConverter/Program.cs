﻿using System;

namespace MenuDrivenTemperatureConverter
{
    
    class Program
    {
        
        static void Main(string[] args)
        {
            string action = "";
            
            Console.WriteLine("Hello World!");
            do
            {
                Console.Write("What would you like to do? CtoF, FtoC or Quit: ");
                try
                {
                    action = Console.ReadLine();
                } 
                catch (FormatException ex)
                {
                    Console.WriteLine("Error - invalid Option " + ex);
                    break;
                }
                               
                
                if (action == "CtoF")
                {
                    MethodCtoF();
                } else 
                if (action == "FtoC")
                {
                    MethodFtoC();
                }


            } while (action != "Quit");

            
        }
        static void MethodCtoF()
        {
            double Celsius = 0;
            double Farenheit = 0;
            Console.WriteLine("Enter the temperature in C");
            try
            {
                Celsius = Convert.ToDouble(Console.ReadLine());
            }
            catch (FormatException ex)
            {
                Console.WriteLine("Error - invalid input " + ex);
                return;

            }
            Farenheit = (1.8 * Celsius) + 32;
            Console.WriteLine(Celsius + " degrees Celsius = " + Farenheit + " degrees Farenheit.");

        }

        static void MethodFtoC()
        {
            double Celsius = 0;
            double Farenheit = 0;
            Console.WriteLine("Enter the temperature in F");
            try
            {
                Farenheit = Convert.ToDouble(Console.ReadLine());
            }
            catch (FormatException ex)
            {
                Console.WriteLine("Error - invalid input " + ex);
                return;

            }
            Celsius = (Farenheit - 32) * (.5556);
            Console.WriteLine(Farenheit + " degrees Farenheit = " + Celsius + " degrees Celsius.");

        }
    }
}
