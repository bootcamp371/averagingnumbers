﻿using System;
using System.IO;

namespace FileTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            string path = @"C:\Users\cn220029\directorysearch";

            if (Directory.Exists(path))
            {
                Console.WriteLine(
                "Directory exists!",
                path);
                //return;
                string[] fileNames = Directory.GetFiles(path);
                foreach (string name in fileNames)
                {
                    Console.WriteLine(name);
                }
            }

            DateTime myNow = DateTime.Now;

           // graceDate.ToString("MM/dd/yyyy")
            string currentFile = @"C:\Users\cn220029\directorysearch\test.txt";
            string newFile = @"C:\Users\cn220029\directorysearch\test-backup" + myNow.ToString("yyyyMMddhhmmss") + ".txt";
            if (!File.Exists(currentFile))
            {
                Console.WriteLine(
                $"Error: {currentFile} doesn't exist!");
                return;
            }
            // Copy the file - overwrite if destination exists
             File.Copy(currentFile, newFile, true);

        }
    }
}
