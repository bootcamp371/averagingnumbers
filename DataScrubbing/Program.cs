﻿using System;

namespace DataScrubbing
{
    class Program
    {
        static void Main(string[] args)
        {
            string another = "N";
            string phone = "";
            char char1 = ' ';
            char replaceWith = ' ';

            Console.WriteLine("Hello World!");

            do
            {

                /* Enter phone 1 */
                Console.Write("Enter Phone number to scub: ");
                phone = Console.ReadLine();
                ScrubPhone(ref phone);
                Console.WriteLine("Scrubbed: " + phone);


                /* Enter Phone with char to remove */
                Console.Write("Enter A Phone number to scub with a character you want to remove: ");
                phone = Console.ReadLine();

                Console.Write("Enter Character you want removed: ");
                char1 = Convert.ToChar(Console.ReadLine());
                ScrubPhone(ref phone, char1);
                Console.WriteLine("Scrubbed: " + phone);

                /* Enter a Phone with a character you want to replace*/
                Console.Write("Enter A Phone number to scub with a character you want to remove: ");
                phone = Console.ReadLine();
                Console.Write("Enter Character you to be replaced: ");
                char1 = Convert.ToChar(Console.ReadLine()); 
                Console.Write("Enter Character you want in its place: ");
                replaceWith = Convert.ToChar(Console.ReadLine()); 
                ScrubPhone(ref phone, char1, replaceWith);
                Console.WriteLine("Scrubbed: " + phone);



                Console.Write("Would you like to enter another Phone number? ");
                another = Console.ReadLine();
            } while (another == "Y");


        }
        public static void ScrubPhone(ref string phoneNumber)
        {
            phoneNumber = phoneNumber.Trim();
        }

        public static void ScrubPhone(ref string phoneNumber, char charToRemove)
        {
            phoneNumber = phoneNumber.Trim();
            phoneNumber = phoneNumber.Replace(charToRemove, '\0');
        }
        public static void ScrubPhone(ref string phoneNumber,
        char charToRemove, char charToReplaceWith)
        {
            phoneNumber = phoneNumber.Trim();
            phoneNumber = phoneNumber.Replace(charToRemove, charToReplaceWith);
        }
    }
}
