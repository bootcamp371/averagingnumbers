﻿using System;

namespace NameParser
{
    class Program
    {
        static void Main(string[] args)
        {
            int space1 = 0;
            int space2 = 0;
            string fullName = "";
            string firstName = "";
            string middleName = "";
            string lastName = "";
            string again = "N";
            do
            {
                Console.Write("Enter in a name to parse: ");
                fullName = Console.ReadLine();

                space1 = fullName.IndexOf(" ");
                space2 = fullName.LastIndexOf(" ");
                /*Console.WriteLine("Space 1: " + space1 + " Space2: " + space2);*/

                firstName = fullName.Substring(0, space1);

                if (space1 != space2)
                {
                    middleName = fullName.Substring(space1 + 1, space2 - space1);
                    lastName = fullName.Substring(space2 + 1);
                }
                else
                {
                    lastName = fullName.Substring(space1 + 1);
                }

                Console.WriteLine("First Name: " + firstName);
                Console.WriteLine("Middle Name: " + middleName);
                Console.WriteLine("Lasst Name: " + lastName);

                Console.Write("Would you like to try again (Y/N) ");
                again = Console.ReadLine();

            } while (again == "Y");

        }
    }
}
